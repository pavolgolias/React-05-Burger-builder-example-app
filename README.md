## JS-05 - React example project - Burger builder
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

See all branches for all features.

#### Used commands and libs:
* create-react-app my-app-name
* npm install
* npm start
* npm install --save radium
* npm run eject
* npm install --save prop-types
* npm install --save axios
* npm install --save react-router-dom
* npm install --save redux
* npm install --save react-redux
* npm install --save redux-thunk
* npm install --save enzyme react-test-renderer enzyme-adapter-react-16
* npm test
* npm remove jest // npm install --save jest
* npm run build
* npm install -g firebase-tools (for example Firebase deployment)
* npm install --save redux-saga
