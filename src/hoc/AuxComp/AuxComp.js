// HOC == higher ordered component
const aux = (props) => props.children;

export default aux;
