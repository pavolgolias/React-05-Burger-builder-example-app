import {all, takeEvery, takeLatest} from 'redux-saga/effects';
import {authCheckStateSaga, authUserSaga, checkAuthTimeoutSaga, logoutSaga} from "./auth";
import {initIngredientsSaga} from "./burgerBuilder";
import {fetchOrdersSaga, purchaseBurgerSaga} from "./order";
import * as actionTypes from "../actions/actionTypes";

export function* watchAuth() {
    yield all([ // ALL means that watchers can run simultaneously (e.g. we can run two async server request simultaneously)
        takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
        takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),
        takeEvery(actionTypes.AUTH_USER, authUserSaga),
        takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga)
    ]);

    // yield takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga);
    // yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga);  // listener to AUTH_INITIATE_LOGOUT to run logoutSaga
    // yield takeEvery(actionTypes.AUTH_USER, authUserSaga);
    // yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga);
}

export function* watchBurgerBuilder() {
    yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
}

export function* watchOrder() {
    yield takeLatest(actionTypes.PURCHASE_BURGER, purchaseBurgerSaga);  // takeLatest will always cancel already running executions and execute only the latest one
    yield takeEvery(actionTypes.FETCH_ORDERS, fetchOrdersSaga);
}
