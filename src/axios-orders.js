import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-burger-react-app.firebaseio.com/'
});

export default instance;
